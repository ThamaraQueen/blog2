<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
     public function index(){
        $nombre = 'Thamara';


        return view('archivo')
        ->with ('nombre', $nombre);
    }

    public function welcome(){
        $nombre = 'Thamara';
        $apellido = 'Jimenez';
        $tel = '477 3948 670';
        $email = 'thamisjz@gmail.com';

        return view('welcome')
        ->with ('nombre', $nombre)
        ->with ('apellido', $apellido)
        ->with ('tel', $tel)
        ->with ('email', $email);
    }
}
